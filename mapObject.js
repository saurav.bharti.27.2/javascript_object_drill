
function mapObject(obj, cb){

    const new_object = {}

    for( const values in obj){
        
        new_object[values] = cb(obj[values], values)
    }

    return new_object;
}
module.exports = mapObject