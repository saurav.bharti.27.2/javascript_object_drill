
function pairs(obj){

    list_of_key_and_value = []

    for(const keys in obj){

        temporary_list = []

        temporary_list.push(keys)
        
        temporary_list.push(obj[keys])

        list_of_key_and_value.push(temporary_list)
    }

    return list_of_key_and_value
}

module.exports = pairs