
const defaults = require('../defaults')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }

const defaultProps = {}

if( !(typeof testObject === 'object' && testObject !== null) && !Array.isArray(testObject)){

    console.log('Error : Please enter valid testObject')
    return

}

if( !(typeof defaultProps === 'object' && defaultProps !== null && !Array.isArray(defaultProps))){

    console.log('Error : Please enter valid testObject')
    return

}


const result = defaults(testObject, defaultProps)

console.log(result)