
const invert = require('../invert.js')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

if( !(typeof testObject === 'object' && testObject!== null && !Array.isArray(testObject)) ){

    console.log(`Please enter valid test Object`)
    
    return
}

const result = invert(testObject)

console.log(result)